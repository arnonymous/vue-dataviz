# vue-dataviz

> A Vue.js demo project to visualize some business data.
>
> The main objective of this project is to demonstrate some data related components (i.e. data-grids), some modules (i.e. vue-resource) and some principles such as JWT authentication.

> The project consists in a Node.js server sub-project and the Vue.js front-end.

> The login/password is **admin/admin**

## Build and run server

``` bash
# go to server sub-folder
cd server

# install dependencies
npm install

# come back to project root
cd ..

# run
node server/server.js
```

## Build and run front

``` bash
# install dependencies
npm install

# run
npm run dev
```
