// const API_URL = 'http://localhost:3001/'
// const LOGIN_URL = API_URL + 'sessions/create/'
// const SIGNUP_URL = API_URL + 'users/'

const LOGIN_URL = 'sessions/create/'
const SIGNUP_URL = 'users/'

export default {

  user: {
    authenticated: false
  },

  login(context, creds, redirect) {

    context.$http.post(LOGIN_URL, creds)
      .then(
        response => {
          localStorage.setItem('id_token', response.data.id_token)

          this.user.authenticated = true

          if(redirect) {
            console.log('inside redirect')
            context.$router.push({ name: 'dashboard' });
          }
        },
        error => {console.log(error)
      });

  },

  logout(context) {
    localStorage.removeItem('id_token')
    this.user.authenticated = false
    // context.$router.push({ name: 'home' });
  },

  checkAuth() {
    var jwt = localStorage.getItem('id_token')
    if(jwt) {
      this.user.authenticated = true
    }
    else {
      this.user.authenticated = false
    }
  },


  getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('id_token')
    }
  }
}
