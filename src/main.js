import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

import auth from './auth'
import { routes } from './routes'

Vue.use(VueResource)
Vue.use(VueRouter)

Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');


Vue.http.options.root = 'http://localhost:3001';
// Vue.http.interceptors.push((request, next) => {
//   console.log(request);
//   next(response => {
//     response.json = () => { return {messages: response.body} }
//   });
// });

// Check the user's auth status when the app starts
auth.checkAuth()

const router = new VueRouter({
  //mode: 'history',
  routes
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
